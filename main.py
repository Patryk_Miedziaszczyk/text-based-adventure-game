import time


def rules():
    print("You can enter uppercase letters.")
    print("Watch out for traps.\n\n")
    time.sleep(3)
    game()


def game():
    checker = 0
    print("You are a flight passenger, you are about to depart, but suddenly a terrorist"
          " in a military disguise runs into the plane and slams the door. Choose what you want to do: ")
    print("A: Try to take the weapon away from the terrorist")
    print("B: Stay tuned for developments")
    print("C: Push the alarm")
    time.sleep(1)
    while checker == 0:
        choice = input("Enter answer: ")
        if choice == "A":
            print("The terrorist shot you, your hand is shot.")
            checker = 1
        elif choice == "B":
            print("The terrorist ordered the pilot to fly immediately and he launched the plane into the air "
                  "without hesitation")
            checker = 1
        elif choice == "C":
            print("You set the alarm on, the terrorist in anger asks who did it?")
            checker = 1


print("The NIGHT GAME")
choice = int(input("Enter 1 if you want to read rules, 2 if you want to start the game.\n"))

if choice == 1:
    rules()
elif choice == 2:
    game()
else:
    print("Error")
